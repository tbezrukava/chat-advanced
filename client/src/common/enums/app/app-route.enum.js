const AppRoute = {
    ANY: '*',
    CHAT: '/',
    LOGIN: '/login',
    USERS: '/users',
    USERS_EDIT_$ID: '/users/edit/:id'
};

export { AppRoute };
