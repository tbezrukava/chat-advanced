import { Route, Routes } from 'react-router-dom';
import { AppRoute } from '../common/enums/app/app-route.enum';
import PublicRoute from './public-route/public-route';
import PrivateRoute from './private-route/private-route';
import NotFound from './not-found/not-found';
import Chat from './chat-view/chat/chat';
import LoginPage from './login-page/login-page';
import UsersPage from './users-view/users-page/users-page';

function App() {
	return (
			<div>
				<Routes>
					<Route path={AppRoute.LOGIN} element={<PublicRoute component={LoginPage} />} />
					<Route exact path={AppRoute.CHAT} element={<PrivateRoute component={Chat} />} />
					<Route path={AppRoute.USERS} element={<PrivateRoute component={UsersPage} />} />
					<Route path={AppRoute.ANY} element={<NotFound />} />
				</Routes>
			</div>
	)
}

export default App;