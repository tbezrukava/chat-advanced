import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { AppRoute } from '../../../common/enums/app/app-route.enum';
import { NavLink } from 'react-router-dom';
import { getLastMessageTime, getParticipantsNum } from '../../../helpers/headerHelpers';

import styles from './styles.module.scss';



const ChatHeader = () => {
    const { messages, isAdmin } = useSelector(state => ({
        messages: state.chat.messages,
        isAdmin: state.profile.user.isAdmin
    }));
    
    const [usersNum, setUsersNum] = useState(0);
    const [messagesNum, setMessagesNum] = useState(0);
    const [lastMessageTime, setLastMessageTime] = useState('');
    const userNoun = usersNum === 1 ? 'participant' : 'participants';
    const userContent = `${usersNum} ${userNoun}`;
    const messageNoun = messagesNum === 1 ? 'message' : 'messages';
    const messageContent = `${messagesNum} ${messageNoun}`;
    const lastMessageTimeContent = `last message ${lastMessageTime}`;
  
    useEffect(() => {
        const usersNum = getParticipantsNum(messages);
        const messagesNum = messages.length;
        const lastMessageTime = getLastMessageTime(messages);
        setUsersNum(usersNum);
        setMessagesNum(messagesNum);
        setLastMessageTime(lastMessageTime);
    },[messages.length]);
    
    const linkToAdminPanel = <NavLink className={styles.link} to={AppRoute.USERS}>Admin panel</NavLink>
    
    const headerInfo = (
        <>
          <div className={styles.info}>
            <div className={`header-users-count`}>{userContent}</div>
            <div className={`header-messages-count`}>{messageContent}</div>
          </div>
          <div className={`header-last-message-date`}>{lastMessageTimeContent}</div>
        </>
    );
    
    return (
        <header className={`header ${styles.header}`}>
            {isAdmin && linkToAdminPanel}
            <div className={`header-title ${styles.title}`}>Work chat</div>
            {messages.length && headerInfo}
        </header>
    );
};

export default ChatHeader;