import { useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { chatActionCreator } from '../../../store/actions';
import Preloader from '../../preloader/preloader';
import ChatHeader from '../chat-header/chat-header';
import EditModal from '../edit-modal/edit-modal';
import styles from './styles.module.scss';
import MessageList from '../message-list/message-list';
import MessageInput from '../message-input/message-input';

const Chat = () =>  {
    const dispatch = useDispatch();
    const { preloader, editMessage, messages, user } = useSelector(state => ({
        preloader: state.chat.preloader,
        editMessage: state.chat.editMessage,
        messages: state.chat.messages,
        user: state.profile.user
    }));
    
    const handleKeyDown = e => {
        if (e.key === 'ArrowDown') {
            const editMessage = [...messages].filter(el => el.userId === user.id).pop();
            dispatch(chatActionCreator.setEditMessage(editMessage));
        }
    }
    
    useEffect(() => {
        dispatch(chatActionCreator.loadMessages());
    }, [dispatch]);
    
    useEffect(() => {
        window.addEventListener('keydown', handleKeyDown);
        
        return () => {
            window.removeEventListener('keydown', handleKeyDown);
        };
    }, [handleKeyDown]);
    
    const chat = (
        <div
            className={styles.chat}
        >
            <ChatHeader />
            <MessageList />
            <MessageInput />
            {editMessage && <EditModal />}
        </div>
    )
    
    
    return (
        <div className="chat">
            {preloader && <Preloader /> || chat}
        </div>
    );
};

export default Chat;