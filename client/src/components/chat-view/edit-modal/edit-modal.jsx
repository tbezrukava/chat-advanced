import { useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { chatActionCreator } from '../../../store/actions';

import styles from './styles.module.scss';

const EditModal = () => {
  const dispatch = useDispatch();
  const { editMessage } = useSelector(state => ({
    editMessage: state.chat.editMessage
  }));
  const textToEdit = editMessage.text;
  const [inputValue, setInputValue] = useState(textToEdit);
  
  const handleClose = useCallback(
      () => dispatch(chatActionCreator.setEditMessage(null)),
      [dispatch]
  );
  
  const handleChange = e => setInputValue(e.target.value);
  
  const handleSubmit = e => {
    e.preventDefault();
    const text = inputValue.trim();
    if (!text) return;
    if (text === editMessage.text) {
      handleClose();
      return;
    }
    const message = { id: editMessage.id, text };
    dispatch(chatActionCreator.editMessage(message));
    handleClose();
  }
  
  return (
      <div className={styles.modal}>
        <div>
          <button
              className={styles.closeBtn}
              onClick={handleClose}
          >
            X
          </button>
          <textarea
              className="message-input-text"
              placeholder="message"
              value={inputValue}
              onChange={handleChange}
          />
          <button
              className={styles.editBtn}
              onClick={handleSubmit}
          >
            Edit
          </button>
        </div>
      </div>
  )
}

export default EditModal;