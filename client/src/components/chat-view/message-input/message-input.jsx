import { useRef, useState } from 'react';
import Icon from '../../icon/icon-button';
import { iconName } from '../../../common/constants/icons';
import styles from './styles.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { Message } from '../../../helpers/new-entities';
import { chatActionCreator } from '../../../store/actions';

const MessageInput = () => {
    const ref = useRef(null);
    const dispatch = useDispatch();
    const { user: { user, id, avatar } } = useSelector(state => ({
        user: state.profile.user
    }));
    const [inputValue, setInputValue] = useState('');
    const handleKeyDown = (e) => {
        if (e.key === 'Enter' && !e.shiftKey) {
            handleSubmit(e);
        }
    }
    
    const handleChange = e => setInputValue(e.target.value);
    
    const handleSubmit = e => {
        e.preventDefault();
        const value = inputValue.trim();
        if (!value) return;
        const newMessage = new Message({
            text: value,
            userId: id,
            user,
            avatar
        });
        dispatch(chatActionCreator.addMessage(newMessage));
        setInputValue('');
    }
    
    return (
        <>
            <form
                className={`message-input ${styles.form}`}
                onSubmit={handleSubmit}
                ref={ref}
                onKeyDown={handleKeyDown}
            >
            <textarea
                className="message-input-text"
                placeholder="message"
                value={inputValue}
                onChange={handleChange}
            />
                <button className="message-input-button">
                    <Icon iconName={iconName.send} />
                </button>
            </form>
        </>
    );
}


export default MessageInput;