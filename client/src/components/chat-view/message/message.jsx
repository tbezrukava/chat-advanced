import Icon from '../../icon/icon-button';
import { iconName } from '../../../common/constants/icons';
import styles from './styles.module.scss';
import { useState } from 'react';

const Message = ({ user, avatar, text, createTime, isEdited }) => {
    const [likeState, setLikeState] = useState('message-like')
    const handleLike = () => {
        likeState === 'message-like' ? setLikeState('message-liked') : setLikeState('message-like');
    }
    
    return (
        <div className={`message ${styles.item}`}>
            <div>
                <img
                    className={`message-user-avatar`}
                    src={avatar}
                    alt="avatar"
                />
                <button
                    className={likeState}
                    onClick={handleLike}
                >
                    <Icon iconName={iconName.like} />
                </button>
                <div className={`message-user-name ${styles.name}`}>{user}</div>
                <div className={`message-text ${styles.text}`}>{text}</div>
                <div className={styles.info}>
                    {isEdited && <span>edited</span>}
                    <span className={`message-time`}>{createTime}</span>
                </div>
            </div>
        </div>
    )
};

export default Message;