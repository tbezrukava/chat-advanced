import Icon from '../../icon/icon-button';
import { iconName } from '../../../common/constants/icons';
import styles from './styles.module.scss';
import { chatActionCreator } from '../../../store/actions';
import { useDispatch } from 'react-redux';
import { useCallback } from 'react';

const OwnMessage = ({ message, isEdited, createTime }) => {
    const dispatch = useDispatch();
    const { text, id } = message;
    const handleEdit = useCallback(() => dispatch(chatActionCreator.setEditMessage(message)),[message]);
    const handleDelete = useCallback(() => dispatch(chatActionCreator.deleteMessage(id)),[id])
    
    return (
        <div className={`own-message ${styles.item}`}>
            <div>
                <div className={styles.panel}>
                    <button
                        className="message-edit"
                        type="button"
                        onClick={handleEdit}
                    >
                        <Icon iconName={iconName.edit} />
                    </button>
                    <button
                        className="message-delete"
                        type="button"
                        onClick={handleDelete}
                    >
                        <Icon iconName={iconName.delete} />
                    </button>
                </div>
                <div className={`message-text ${styles.text}`}>{text}</div>
                <div className={styles.info}>
                    {isEdited && <span>edited</span>}
                    <span className={`message-time`}>{createTime}</span>
                </div>
            </div>
        </div>
    );
}

export default OwnMessage;