import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Icon = ({ iconName }) => <FontAwesomeIcon icon={iconName}/>

export default Icon;