import { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';
import { profileActionCreator } from '../../store/actions'
import Icon from '../icon/icon-button';
import { iconName } from '../../common/constants/icons';

import styles from './styles.module.scss';

const LoginPage = () => {
  const dispatch = useDispatch();
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  
  const handleLoginChange = (e) => {
    setLogin(e.target.value);
  }
  
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  }
  
  const handleLogin = useCallback(
      loginPayload => dispatch(profileActionCreator.login(loginPayload)),
      [dispatch]
  );
  
  const onLogin = (e) => {
    e.preventDefault();
    if (login && password) handleLogin({ login, password });
  }
  
    return (
        <div className={styles.loginPage}>
            <form
                onSubmit={onLogin}
            >
                <input
                    onChange={handleLoginChange}
                    type="text"
                    placeholder="login"/>
                <input
                    onChange={handlePasswordChange}
                    type="password"
                    placeholder="password" />
                <button
                    className="message-input-button"
                    type="submit"
                >
                    <Icon iconName={iconName.send} />
                </button>
            </form>
        </div>
    );
}
export default LoginPage;