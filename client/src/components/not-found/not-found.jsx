import styles from './styles.module.scss';

const NotFound = () => (
  <h2 className={styles.title}>
    <span className={styles.code}>404 Not Found</span>
  </h2>
);

export default NotFound;
