import { Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { AppRoute } from '../../common/enums/app/app-route.enum';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  
  const hasUser = Boolean(user);

  return hasUser
    ? <Component {...rest} />
    : <Navigate to={{ pathname: AppRoute.LOGIN, state: { from: rest.location } }} />;
};

export default PrivateRoute;
