import { Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { AppRoute } from '../../common/enums/app/app-route.enum';

const PublicRoute = ({ component: Component, ...rest }) => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  
  const hasUser = Boolean(user);
  let redirectPath = AppRoute.CHAT;
  if (user?.isAdmin) {
    redirectPath = AppRoute.USERS
  }
  
  return hasUser
    ? <Navigate to={{ pathname: redirectPath, state: { from: rest.location } }} />
    : <Component {...rest} />;
};

export default PublicRoute;
