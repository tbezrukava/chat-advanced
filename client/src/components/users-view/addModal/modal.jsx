import styles from './styles.module.scss';

const Modal = () => {
  return (
      <div className={styles.modal}>
        <div>
          <button
              className={styles.closeBtn}
              onclose={() => {}}
          >
            X
          </button>
        </div>
      </div>
  )
}

export default Modal;