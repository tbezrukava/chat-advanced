import { useSelector } from 'react-redux';
import User from '../user/user';
import styles from './styles.module.scss'

const UserList = () => {
  const { users } = useSelector(state => ({
    users: state.users.users
  }));
  
  const usersList = [...users].map(el => {
    const itemContent = (
        <User  {...el} />
    );
    return <li key={el.id}>{itemContent}</li>
  })
  
  return (
      <ul className={styles.list}>
        {usersList}
      </ul>
  )
}

export default UserList;