import Icon from '../../icon/icon-button';
import { iconName } from '../../../common/constants/icons';
import styles from './styles.module.scss';

const User = ({user, email, id}) => {
  const handleDelete = () => {console.log('delete', id)};
  const handleEdit = () => {console.log('edit', id)};
  
  return (
      <div className={styles.item}>
        <span>{user}</span>
        <span>{email}</span>
        <div>
          <button
              className="message-edit"
              type="button"
              onClick={handleEdit}
          >
            <Icon iconName={iconName.edit} />
          </button>
          <button
              className="message-delete"
              type="button"
              onClick={handleDelete}
          >
            <Icon iconName={iconName.delete} />
          </button>
        </div>
      </div>
  );
}

export default User;