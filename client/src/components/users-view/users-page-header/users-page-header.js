import { AppRoute } from '../../../common/enums/app/app-route.enum';
import { NavLink } from 'react-router-dom';

import styles from './styles.module.scss';

const UsersPageHeader = () => {
  const linkToChat = <NavLink className={styles.link} to={AppRoute.CHAT}>Chat</NavLink>
  
  return (
      <header className={`header ${styles.header}`}>
        {linkToChat}
        <div className={styles.title}>Admin Panel</div>
      </header>
  );
};

export default UsersPageHeader;