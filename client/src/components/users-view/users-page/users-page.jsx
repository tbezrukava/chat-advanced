import UsersPageHeader from '../users-page-header/users-page-header';
import UserList from '../user-list/user-list';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { usersActionCreator } from '../../../store/actions';
import Preloader from '../../preloader/preloader';

const UsersPage = () => {
  const dispatch = useDispatch();
  const { preloader } = useSelector(state => ({
    preloader: state.users.preloader
  }))
  
  useEffect(() => {
    dispatch(usersActionCreator.loadUsers());
  }, [dispatch]);
  
  const users = (
      <div>
        <UsersPageHeader />
        <UserList />
      </div>
  );
  
  return (
      <div>
        {preloader && <Preloader /> || users}
      </div>
  );
}

export default UsersPage;