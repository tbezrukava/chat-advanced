import { uniq } from 'lodash';
import { dayjs } from './dayjs'

const getParticipantsNum = (messages)=> uniq(messages.map(el => el.user)).length;

const getLastMessageTime =  (messages) => {
		if (!messages) {
				return '';
		}
		const lastMessageDate = [...messages].map(el => el.createdAt).sort()[messages.length - 1];
		return convertDate(lastMessageDate);
};

const convertDate = (fullDate) => {
		const today = dayjs().format('DD.MM.YYYY');
		const yesterday = dayjs().subtract(1, 'day').format('DD.MM.YYYY');
		const date = dayjs.utc(fullDate).format('DD.MM.YYYY');
		const time = dayjs.utc(fullDate).format('HH:mm');
		let day;
		switch (date) {
				case today:
						day = '';
						break;
				case yesterday:
						day = yesterday;
						break;
				default:
						day = date;
		}
		
		return `${day} at ${time}`
};

export { getParticipantsNum, getLastMessageTime };