class Message {
    constructor({ userId, avatar, user, text }) {
        this.userId = userId;
        this.avatar = avatar;
        this.user = user;
        this.text = text;
    }
}

export { Message };