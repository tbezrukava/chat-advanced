import { HttpMethod, ContentType } from '../../common/enums/enums';

class Auth {
    constructor({ apiPath, http }) {
        this._apiPath = apiPath;
        this._http = http;
    }
  
    login(payload) {
        return this._http.load(`${this._apiPath}/auth/login`, {
            method: HttpMethod.POST,
            contentType: ContentType.JSON,
            payload: JSON.stringify(payload)
        });
    }
}

export { Auth };
