import { HttpMethod, ContentType } from '../../common/enums/enums';

class Message {
    constructor({ apiPath, http }) {
        this._apiPath = apiPath;
        this._http = http;
    }
  
    getAllMessages() {
        return this._http.load(`${this._apiPath}/messages`, {
            method: HttpMethod.GET
        });
    }
  
    getMessage(id) {
        return this._http.load(`${this._apiPath}/messages/${id}`, {
            method: HttpMethod.GET
        });
    }
  
    addMessage(payload) {
        return this._http.load(`${this._apiPath}/messages`, {
            method: HttpMethod.POST,
            contentType: ContentType.JSON,
            payload: JSON.stringify(payload)
        });
    }
  
    updateMessage(id, payload) {
        return this._http.load(`${this._apiPath}/messages/${id}`, {
            method: HttpMethod.PUT,
            contentType: ContentType.JSON,
            payload: JSON.stringify(payload)
        });
    }
    
    deleteMessage(id) {
        return this._http.load(`${this._apiPath}/messages/${id}`, {
            method: HttpMethod.DELETE
        });
    }
}

export { Message };
