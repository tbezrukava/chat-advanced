import { Http } from './http/http.service';
import { Message } from './message/message.service';
import { User } from './user/user.service';
import { Auth } from  './auth/auth.service';


const apiPath = 'http://localhost:3050/api';

const http = new Http();
const message = new Message({
		apiPath,
		http
});

const user = new User({
		apiPath,
		http
});

const auth = new Auth({
		apiPath,
		http
});

export { message, user, auth };