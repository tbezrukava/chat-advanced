import { HttpMethod, ContentType } from '../../common/enums/enums';

class User {
    constructor({ apiPath, http }) {
        this._apiPath = apiPath;
        this._http = http;
    }
  
    getAllUsers() {
        return this._http.load(`${this._apiPath}/users`, {
            method: HttpMethod.GET
        });
    }
    
    addUser(payload) {
        return this._http.load(`${this._apiPath}/users`, {
            method: HttpMethod.POST,
            contentType: ContentType.JSON,
            payload: JSON.stringify(payload)
        });
    }
  
    updateUser(id, payload) {
        return this._http.load(`${this._apiPath}/users/${id}`, {
            method: HttpMethod.PUT,
            contentType: ContentType.JSON,
            payload: JSON.stringify(payload)
        });
    }
    
    deleteUser(id) {
        return this._http.load(`${this._apiPath}/users/${id}`, {
            method: HttpMethod.DELETE
        });
    }
}

export { User };
