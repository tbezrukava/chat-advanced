import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadMessages = createAsyncThunk(
		ActionType.LOAD_MESSAGES,
		async (_request, { extra: { services } }) => {
				const messages = await services.message.getAllMessages();
				return { messages };
		}
);

const addMessage = createAsyncThunk(
		ActionType.ADD_MESSAGE,
		async (request, { extra: { services } }) => {
				const message = await services.message.addMessage(request);
				return { message };
		}
);

const setEditMessage = createAsyncThunk(
		ActionType.SET_EDIT_MESSAGE, (request) => {
				return ({ editMessage: request });
		}
);

const editMessage = createAsyncThunk(
		ActionType.EDIT_MESSAGE,
		async ({ id, text }, { getState, extra: { services } }) => {
				const message = await services.message.updateMessage(id, { text });
				const { chat: {messages} } = getState();
				const editMessage = messages.find(el => el.id === message.id);
				const editMessageIdx = messages.indexOf(editMessage);
				const updateMessages = (
						[
							...messages.slice(0, editMessageIdx),
							message,
							...messages.slice(editMessageIdx + 1)
						]
				)
			return { messages: updateMessages };
		}
);

const deleteMessage = createAsyncThunk(
		ActionType.DELETE_MESSAGE,
		async (request, { getState, extra: { services } }) => {
			await services.message.deleteMessage(request);
			const { chat: {messages} } = getState();
			const editMessage = messages.find(el => el.id === request);
			const editMessageIdx = messages.indexOf(editMessage);
			const updateMessages = (
					[
						...messages.slice(0, editMessageIdx),
						...messages.slice(editMessageIdx + 1)
					]
			)
			return { messages: updateMessages };
		});

export { loadMessages, addMessage, setEditMessage, editMessage, deleteMessage }