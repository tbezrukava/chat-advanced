const ActionType = {
		LOAD_MESSAGES: 'chat/load-messages',
		ADD_MESSAGE: 'chat/add_message',
		SET_EDIT_MESSAGE: 'chat/set_edit_message',
		EDIT_MESSAGE: 'chat/edit_message',
		DELETE_MESSAGE: 'chat/delete_message'
}

export { ActionType };