import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import * as chatActions from './actions';

const initialState = {
		messages: [],
		editMessage: null,
		preloader: true
};

const reducer = createReducer(initialState, builder => {
		builder.addCase(chatActions.loadMessages.fulfilled, (state, action) => {
				const { messages } = action.payload;
				state.messages = messages;
				state.preloader = false;
		});
		builder.addCase(chatActions.addMessage.fulfilled, (state, action) => {
				const { message } = action.payload;
				state.messages = [...state.messages, message];
		});
		builder.addCase(chatActions.setEditMessage.fulfilled, (state, action) => {
				const { editMessage } = action.payload;
				state.editMessage = editMessage;
		});
		builder.addMatcher(isAnyOf(
				chatActions.editMessage.fulfilled,
				chatActions.deleteMessage.fulfilled
		), (state, action) => {
				const { messages } = action.payload;
				state.messages = messages;
		});
});

export { reducer };