import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const login = createAsyncThunk(
    ActionType.LOG_IN,
    async (request, { extra: { services } }) => {
          const user = await services.auth.login(request);
          return user;
    }
);

export { login };
