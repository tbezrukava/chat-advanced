import { createReducer } from '@reduxjs/toolkit';
import * as profileActions from './actions';

const initialState = {
    user: null
};

const reducer = createReducer(initialState, builder => {
    builder
        .addCase(profileActions.login.fulfilled, (state, action) => {
            state.user = action.payload;
        })
        .addCase(profileActions.login.rejected, (state, action) => {
            state.user = null;
        });
});

export { reducer };
