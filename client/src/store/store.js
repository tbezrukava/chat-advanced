import { configureStore } from '@reduxjs/toolkit';

import * as services from '../services/services';
import { chatReducer, usersReducer, profileReducer } from './root-reducer';

const store = configureStore({
    reducer: {
        chat: chatReducer,
        users: usersReducer,
        profile: profileReducer
    },
    middleware: getDefaultMiddleware => (getDefaultMiddleware({
        thunk: {
            extraArgument: { services }
        }
    }))
});

export default store;
