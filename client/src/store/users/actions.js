import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadUsers = createAsyncThunk(
		ActionType.LOAD_USERS,
		async (_request, { extra: { services } }) => {
				const users = await services.user.getAllUsers();
				return { users };
		}
);

export { loadUsers };