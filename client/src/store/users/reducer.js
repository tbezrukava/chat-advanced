import { createReducer } from '@reduxjs/toolkit';
import * as usersActions from './actions';

const initialState = {
		users: [],
		preloader: true
};

const reducer = createReducer(initialState, builder => {
		builder.addCase(usersActions.loadUsers.fulfilled, (state, action) => {
				const { users } = action.payload;
				
				state.users = users;
				state.preloader = false;
		});
});

export { reducer };