const { errors } = require('../constants/errors');

const responseMiddleware = (req, res, next) => {
    if(res.err) {
        const status = errors.find(it => it.message === res.err.message).status;
        res.status(status);
        res.json({error: true, message: res.err.message});
        res.send();
    } else {
        res.status(200).json(res.data);
        next();
    }
}

exports.responseMiddleware = responseMiddleware;