const { user } = require('../models/users-page');
const { validateCreateEntity, validateUpdateEntity } = require('../helpers/validationHelper');

const createUserValid = (req, res, next) => {
    const data = {
        req,
        res,
        next,
        validationFunction: fieldValid,
        modelEntity: user
    };
    
    validateCreateEntity(data);
}

const updateUserValid = (req, res, next) => {
    const data = {
        req,
        res,
        next,
        validationFunction: fieldValid,
    };
    
    validateUpdateEntity(data);
}

const nameValid = (name = '') => !!name.trim().length;
const emailValid = (email = '') => email.toLocaleLowerCase().endsWith('@gmail.com');
const phoneNumberValid = (phoneNumber = '') => /\+380\d{9}/.test(phoneNumber) && phoneNumber.length === 13;
const passwordValid = (password = '') => password.length >= 3;

function fieldValid(user, field) {
    const { firstName, lastName, password, email, phoneNumber } = user;
    let isValid;
    
    switch (field) {
        case 'firstName':
            isValid = nameValid(firstName);
            break;
        case 'lastName':
            isValid = nameValid(lastName);
            break;
        case 'email':
            isValid = emailValid(email);
            break;
        case 'phoneNumber':
            isValid = phoneNumberValid(phoneNumber);
            break;
        case 'password':
            isValid = passwordValid(password);
            break;
        default:
            isValid = false;
    }
    
    return isValid;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;