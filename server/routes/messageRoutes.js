const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function(req, res, next) {
    try {
        res.data = MessageService.getAllMessages();
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
    try {
        const messageId = req.params.id;
        res.data = MessageService.getMessage(messageId);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', function(req, res, next) {
    try {
        res.data = MessageService.createMessage(req.body)
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', function(req, res, next) {
    try {
        const messageId = req.params.id;
        const dataToUpdate = req.body;
        res.data = MessageService.updateMessage(messageId, dataToUpdate);
    } catch (err) {
        res.err = err;
    } finally {
        next();
  }
}, responseMiddleware);

router.delete('/:id', function(req, res, next) {
    const messageId = req.params.id;
    res.data = MessageService.deleteMessage(messageId);
    next();
}, responseMiddleware);

module.exports = router;