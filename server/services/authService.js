const UserService = require('./userService');

class AuthService {
    login(userData) {
        const user = UserService.search(userData);
        if(!user) {
            throw Error('User not found');
        }
        return {
            user: user.user,
            avatar: user.avatar,
            email: user.email,
            isAdmin: user.login === 'admin',
            id: user.id
        };
    }
}

module.exports = new AuthService();