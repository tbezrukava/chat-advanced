const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
    search(search) {
        const item = MessageRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    
    getMessage(messageId) {
        const user = this.search(it => it.id === messageId);
        if(!user) {
            throw Error('Message not found');
        }
        return user;
    }
    
    getAllMessages() {
        const messages = MessageRepository.getAll();
        if(!messages) {
            throw Error('Request error');
        }
        return messages;
    }
    
    createMessage(data) {
        return  MessageRepository.create(data);
    }
    
    updateMessage(messageId, dataToUpdate) {
        return MessageRepository.update(messageId, dataToUpdate)
    }
    
    deleteMessage(messageId) {
        MessageRepository.delete(messageId);
        return {};
    }
}

module.exports = new MessageService();