const { UserRepository } = require('../repositories/userRepository');

class UserService {
    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    
    getUser(userId) {
        const user = this.search(it => it.id === userId);
        if(!user) {
            throw Error('User not found');
        }
        return user;
    }
    
    getAllUsers() {
        const users = UserRepository.getAll();
        if(!users) {
            throw Error('Request error');
        }
        return users;
    }
    
    createUser(data) {
        const emailAlreadyExist = this.search(it => it.email === data.email);
        const loginAlreadyExist = this.search(it => it.login === data.login);
        const userAlreadyExist = emailAlreadyExist || loginAlreadyExist;
        if (userAlreadyExist) {
            throw Error('User has already exist')
        }
        return  UserRepository.create(data);
    }
    
    updateUser(userId, dataToUpdate) {
        if(Object.keys(dataToUpdate).includes('email')) {
            if (this.search(it => it.email === dataToUpdate.email)) {
                throw Error('User has already exist')
            }
        }
        if(Object.keys(dataToUpdate).includes('login')) {
            if (this.search(it => it.login === dataToUpdate.login)) {
                throw Error('User has already exist')
            }
        }
        return UserRepository.update(userId, dataToUpdate)
    }
    
    deleteUser(userId) {
        UserRepository.delete(userId);
        return {};
    }
}

module.exports = new UserService();